package com.glar.tsat.exceptions;

/**
 * Created by adyag on 09/17/2015.
 */
public class TSMethodNotSupportedException extends Exception {

    public TSMethodNotSupportedException(String detailMessage) {
        super(detailMessage);
    }
}
