package com.glar.tsat.fntools;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

public class TSFragmentsPagerAdapter extends FragmentStatePagerAdapter {
    private List<TSFragment> fragments;
    private String[] titles;

    public TSFragmentsPagerAdapter(FragmentManager fm, List<TSFragment> fragmentList,
                                   String[] titles) {
        super(fm);
        fragments = fragmentList;
        this.titles = titles;
    }

    @Override
    public Fragment getItem(int i) {
        return fragments.get(i);
    }

    @Override
    public Parcelable saveState() {
        return null;                // Fix for numerous crashes caused by this adapter. This is working!! REALLY!!! :)
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

}
