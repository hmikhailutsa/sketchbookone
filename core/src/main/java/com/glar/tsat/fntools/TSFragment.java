package com.glar.tsat.fntools;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.glar.tsat.exceptions.TSMethodNotSupportedException;

/**
 * Created by AdYa for simplifying the fragment management on Android platforms.
 * Ready-to-use fragment with automated typical actions.
 * Used only with TSActivity.
 * <p/>
 * Current version: 2.2.0
 * Release date: 21.10.2014
 * Copyright (C) 2014 Fragment Navigation Tools.
 */
public abstract class TSFragment extends Fragment {

    private View parentView;
    private StateSaver stateSaver = null;

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(getLayoutResourceID(), container, false);
    }

    @Override
    public final void onViewCreated(View parentView, Bundle savedInstanceState) {
        super.onViewCreated(parentView, savedInstanceState);
        this.parentView = parentView;
        setLocked(false);
        initializeFragmentView(parentView, savedInstanceState);
        if (stateSaver != null) {
            SharedPreferences prefs = getTSActivity().getSharedPreferences(getPrefsName(), Context.MODE_PRIVATE);
            stateSaver.restoreState(prefs);
            prefs.edit().clear().apply();
        }
    }

    /** Returns fragment's view. */
    protected View getParentView() {
        return parentView;
    }

    /**
     * Override this method to specify a way to refresh a view.
     */
    protected void updateView() throws TSMethodNotSupportedException {
        throw new TSMethodNotSupportedException(this.getClass().getName() + ": updateView() not implemented.");
    }

    /**
     * Returns parent activity.
     */
    public final TSActivity getTSActivity() {
        return (TSActivity) getActivity();
    }

    /**
     * Returns name of the preferences file.
     */
    private String getPrefsName() {
        return getClass().getName() + "_" + getTSActivity().getContainerID() + "_state";
    }

    /**
     * Checks whether activity, to which fragment is being attached, is instance of TSActivity class. It is necessary condition for correct work.
     */
    @Override
    public void onAttach(Activity activity) {
        if (!(activity instanceof TSActivity))
            throw new IllegalArgumentException("TSFragment must be attached to a TSActivity.");
        super.onAttach(activity);
        if (getTitle() != null && getFragmentManager().equals(getTSActivity().getSupportFragmentManager())) { // if it is the main fragment
            getTSActivity().setTitle(getTitle());
        }
    }

    /**
     * Takes care about "unlocking" UI elements when fragment is resuming.
     */
    @Override
    public void onResume() {
        super.onResume();
        setLocked(false);
    }

    /**
     * Saves fragment's state when it is about to pause.
     */
    @Override
    public void onPause() {
        if (stateSaver != null) {
            SharedPreferences.Editor editor = getTSActivity().getSharedPreferences(getPrefsName(), Context.MODE_PRIVATE).edit();
            stateSaver.saveState(editor);
            editor.apply();
        }
        super.onPause();
    }

    /**
     * Set up stateSaver to save and restore fragments state.
     */
    protected final void setStateSaver(StateSaver stateSaver) {
        this.stateSaver = stateSaver;
    }

    /**
     * Locks UI.
     * Override this method if you want to implement custom disabling/enabling of fragment's UI elements.
     */
    public void setLocked(boolean isLocked) {
        if (isVisible()) {
            getTSActivity().setLoading(isLocked);
            View v = getTSActivity().getContainerView();
            if (v != null)
                v.setVisibility(isLocked ? View.INVISIBLE : View.VISIBLE);
        }
    }

    /**
     * Returns fragment's title which can be displayed by the TSActivity.
     * If no title needed return null.
     */
    public String getTitle() {
        return null;
    }

    /**
     * Calls when fragment initializing its' view.
     * Override one of the following methods.
     */
    protected void initializeFragmentView(View parentView, Bundle savedInstanceState) {
    }

    /**
     * Should return layout resource id, which will be inflated for the fragment.
     */
    protected abstract int getLayoutResourceID();
}
