package com.glar.tsat;

public class TSTimer {
    static long captured = 0;

    public static long measure(){
        long cur = System.currentTimeMillis();
        long measured = (captured == 0 ? 0 : cur - captured);
        captured = cur;
        return measured;
    }
}
