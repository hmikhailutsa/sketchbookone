package com.hlmi.sketchbook.core.adapters;

import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.glar.tsat.fntools.TSFragment;
import com.hlmi.sketchbook.core.R;
import com.hlmi.sketchbook.managers.SBBooksManager;
import com.hlmi.sketchbook.models.SBBook;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersBaseAdapter;

import java.util.ArrayList;

import static com.hlmi.sketchbook.models.SBBook.SBBookCategory;

public class SBBooksAdapter extends BaseAdapter implements StickyGridHeadersBaseAdapter{

    private TSFragment fragment;
    private ArrayList<SBBook> basics;
    private ArrayList<SBBook> history;
    private ArrayList<SBBook> tech;

    ///TODO: Rewrite books categories
//    private HashMap<String, ArrayList<SBBook>> categories;

    private final String HEADERS[] = new String[] {"Basics", "Techniques", "History"};

    public SBBooksAdapter(TSFragment fragment/*, HashMap<String, ArrayList<SBBook>> categories*/) {
        super();
        this.fragment = fragment;
        //this.categories = categories;
        basics = SBBooksManager.getManager().getBooks(SBBookCategory.Basics);
        tech = SBBooksManager.getManager().getBooks(SBBookCategory.Techniques);
        history = SBBooksManager.getManager().getBooks(SBBookCategory.History);
    }

    @Override
    public int getCount() {
        return basics.size() + history.size() + tech.size();
    }

    @Override
    public SBBook getItem(int i) {
        if (i >= basics.size()){
            i-= basics.size();
            if (i >= tech.size()) {
                i -= tech.size();
                return history.get(i);
            }
            else
                return tech.get(i);
        }
        else
            return basics.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View v = convertView;
        if (v == null){
            v = LayoutInflater.from(fragment.getContext()).inflate(R.layout.item_book, null, false);
        }
        TextView tvName = (TextView) v.findViewById(R.id.tvBookName);
        TextView tvAuthors = (TextView) v.findViewById(R.id.tvBookAuthors);
        ImageView ivImage = (ImageView) v.findViewById(R.id.ivBookImage);

        SBBook book = getItem(position);
        tvName.setText(book.getName());
        tvAuthors.setText(book.getAuthors());
        ivImage.setImageResource(book.getDrawable());

        int textColor;

        if (SBBooksManager.getManager().isAvailableBook(book)) {
            textColor = ContextCompat.getColor(fragment.getContext(), book.getTextColor());
            v.setBackgroundColor(ContextCompat.getColor(fragment.getContext(), book.getAccentColor()));
            ivImage.setColorFilter(null);
        }
        else {
            textColor = ContextCompat.getColor(fragment.getContext(), R.color.darkGray);
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0);
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            ivImage.setColorFilter(filter);
            v.setBackgroundColor(ContextCompat.getColor(fragment.getContext(), R.color.dimmed));
        }

        tvName.setTextColor(textColor);
        tvAuthors.setTextColor(textColor);
        return v;
    }

    @Override
    public int getCountForHeader(int i) {
        switch (i){
            case 0: return basics.size();
            case 1: return tech.size();
            case 2: return history.size();
            default: return 0;
        }
    }

    @Override
    public int getNumHeaders() {
        return 3;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup viewGroup) {
        View v = convertView;
        if (v == null){
            v = LayoutInflater.from(fragment.getContext()).inflate(R.layout.item_header, null, false);
        }
        ((TextView) v.findViewById(R.id.tvHeader)).setText(HEADERS[position]);
        return v;
    }
}
