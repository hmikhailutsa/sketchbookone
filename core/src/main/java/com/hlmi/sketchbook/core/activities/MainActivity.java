package com.hlmi.sketchbook.core.activities;

import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import com.astuetz.PagerSlidingTabStrip;
import com.glar.tsat.fntools.TSActivity;
import com.glar.tsat.fntools.TSFragment;
import com.glar.tsat.fntools.TSFragmentsPagerAdapter;
import com.hlmi.sketchbook.core.R;
import com.hlmi.sketchbook.core.fragments.BooksFragment;
import com.hlmi.sketchbook.core.fragments.ProjectsFragment;

import java.util.Arrays;

public class MainActivity extends TSActivity {

    private PagerSlidingTabStrip tabs;
    private ViewPager viewPager;

    private TSFragmentsPagerAdapter fragmentAdapter;
    private String[] fragmentTitles;


    @Override
    protected void onTSActivityCreated(View activityView) {
        setupTabsAndViewPager();
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.logo_white); //also displays wide logo
        getSupportActionBar().setDisplayShowTitleEnabled(false); //optional
    }

    private void setupTabsAndViewPager() {
        fragmentTitles = new String[]{"Projects", "Painting Tutorials", "Books"};
        tabs = (PagerSlidingTabStrip) findViewById(R.id.tsMainTabs);
        tabs.setShouldExpand(true);
        tabs.setBackgroundColor(ContextCompat.getColor(this, R.color.tabs_background));
        tabs.setIndicatorColorResource(R.color.tabs_indicator);
        tabs.setTextColorResource(R.color.tabs_text_title);
        //tabs.setTypeface(TypefaceManager.getInstance().getDefaultTypeface(TypefaceManager.TypefacesStyle.NORMAL), Typeface.NORMAL);
        viewPager = (ViewPager) findViewById(R.id.vpMainContainer);

        TSFragment fragments[] = new TSFragment[]{new ProjectsFragment(ProjectsFragment.Type.PROJECTS), new ProjectsFragment(ProjectsFragment.Type.TUTORIALS), new BooksFragment()};
        fragmentAdapter = new TSFragmentsPagerAdapter(getSupportFragmentManager(), Arrays.asList(fragments), fragmentTitles);
        viewPager.setAdapter(fragmentAdapter);
        tabs.setViewPager(viewPager);
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
        viewPager.setPageMargin(pageMargin);
    }

    @Override
    protected int getContainerID() {
        return 0;
    }

    @Override
    protected int getLayoutResourceID() {
        return R.layout.activity_main;
    }

    @Override
    public <T extends Fragment> Class<T> getInitialFragmentClass() {
        return null;
    }
}

