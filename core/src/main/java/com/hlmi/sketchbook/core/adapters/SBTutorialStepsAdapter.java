package com.hlmi.sketchbook.core.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.hlmi.sketchbook.core.R;
import com.hlmi.sketchbook.models.SBTutorialStep;

import java.util.List;

public class SBTutorialStepsAdapter extends BaseAdapter {

    private Context context;
    private List<SBTutorialStep> steps;

    public SBTutorialStepsAdapter(Context context, List<SBTutorialStep> steps) {
        this.context = context;
        this.steps = steps;
    }

    @Override
    public int getCount() {
        return steps.size();
    }

    @Override
    public SBTutorialStep getItem(int i) {
        return steps.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        if (v == null){
            v = LayoutInflater.from(context).inflate(R.layout.item_tutorial_step, null, false);
        }

        Button bStep = (Button) v.findViewById(R.id.bStep);
        TextView tvStep = (TextView) v.findViewById(R.id.tvStep);

        bStep.setText(String.valueOf(i + 1));
        tvStep.setText(getItem(i).getName());

        bStep.setEnabled(getItem(i).isCurrent());
        tvStep.setEnabled(getItem(i).isCurrent());

        v.findViewById(R.id.vSpacer).setVisibility(i == getCount() - 1 ? View.GONE : View.VISIBLE);
        return v;
    }
}
