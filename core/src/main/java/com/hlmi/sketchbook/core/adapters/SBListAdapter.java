package com.hlmi.sketchbook.core.adapters;

import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.hlmi.sketchbook.core.R;

import java.util.List;

public class SBListAdapter extends BaseAdapter {

    private Context context;
    private List<Pair<Integer, String>> items;

    public SBListAdapter(Context context, List<Pair<Integer, String>> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View v = convertView;
        if (v == null){
            v = LayoutInflater.from(context).inflate(R.layout.item_more, null, false);
        }
        ImageView ivMore = (ImageView) v.findViewById(R.id.ivMoreIcon);
        TextView tvMore = (TextView) v.findViewById(R.id.tvMoreTitle);

        Pair<Integer, String> item = items.get(position);

        ivMore.setImageResource(item.first);
        tvMore.setText(item.second);

        return v;
    }
}
