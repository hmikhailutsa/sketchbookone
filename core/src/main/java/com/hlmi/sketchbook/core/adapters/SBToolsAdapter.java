package com.hlmi.sketchbook.core.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.hlmi.sketchbook.core.R;
import com.hlmi.sketchbook.models.tools.SBTool;

public class SBToolsAdapter extends BaseAdapter {

    private SBTool[] tools;
    private Context context;

    public SBToolsAdapter( Context context, SBTool[] tools) {
        this.tools = tools;
        this.context = context;
    }

    @Override
    public int getCount() {
        return tools.length;
    }

    @Override
    public Object getItem(int i) {
        return tools[i];
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View v = convertView;
        if (v == null){
            v = LayoutInflater.from(context).inflate(R.layout.item_tool, null, false);
        }
        ((ImageView) v.findViewById(R.id.ivTool)).setImageResource(tools[position].getIcon());
        return v;
    }
}
