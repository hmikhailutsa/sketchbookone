package com.hlmi.sketchbook.core.adapters;

import android.util.Pair;
import android.view.*;
import android.widget.*;
import com.glar.tsat.fntools.TSFragment;
import com.hlmi.sketchbook.core.R;
import com.hlmi.sketchbook.managers.SBProjectsManager;
import com.hlmi.sketchbook.models.SBProject;

import java.util.ArrayList;
import java.util.List;

public class SBProjectAdapter extends BaseAdapter {
    private SBProject[] projects;

    private int currentProjectIndex;
    private TSFragment fragment;
    private boolean isEditable;

    public SBProjectAdapter(TSFragment fragment, List<SBProject> projects, boolean isEditable){
        this.fragment = fragment;
        this.isEditable = isEditable;
        this.projects = projects.toArray(new SBProject[0]);
    }

    @Override
    public int getCount() {
        return projects.length;
    }

    @Override
    public Object getItem(int i) {
        return projects[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null){
            v = LayoutInflater.from(fragment.getContext()).inflate(R.layout.item_project, null, false);
        }
        ImageView ivPicture = (ImageView) v.findViewById(R.id.ivItemPicture);
        TextView tvTitle = (TextView) v.findViewById(R.id.tvItemTitle);
        SBProject project = projects[position];
        ivPicture.setImageBitmap(project.getThumbnail());
        tvTitle.setText(project.getName());
        final ImageButton ibMore = (ImageButton) v.findViewById(R.id.ibExport);
        ibMore.setVisibility(isEditable ? View.VISIBLE : View.GONE);
        if (isEditable) {
            ibMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ListPopupWindow window = createMorePopup();
                    window.setAnchorView(ibMore);
                    window.show();
                    currentProjectIndex = position;
                }
            });
        }
        return v;
    }

    private ListPopupWindow createMorePopup(){
        final ListPopupWindow window = new ListPopupWindow(fragment.getContext());
        ArrayList<Pair<Integer, String>> items = new ArrayList<>();
        items.add(new Pair<>(R.drawable.ic_edit_black_24dp, "Rename"));
        items.add(new Pair<>(R.drawable.ic_delete_black_24dp, "Remove"));
        window.setAdapter(new SBListAdapter(fragment.getContext(), items));
        window.setWidth(250); /// TODO: Kostil
        window.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0: {
                        window.dismiss();
                        createRenamePopup().showAtLocation(fragment.getView(), Gravity.CENTER, 0, 0);
                        break;
                    }
                    case 1: SBProjectsManager.getManager().removeProject(currentProjectIndex); window.dismiss(); break;
                    default: window.dismiss();
                }
            }
        });
        return window;
    }

    private PopupWindow createRenamePopup() {
        final View popupView = View.inflate(fragment.getContext(), R.layout.popup_rename_project, null);
        final PopupWindow window = new PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true);
        final EditText etEdit = (EditText) popupView.findViewById(R.id.etProjectName);
        etEdit.setText(projects[currentProjectIndex].getName());
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                window.dismiss();
                return true;
            }
        });

        Button bCancel = (Button) popupView.findViewById(R.id.bCancel);
        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                window.dismiss();
            }
        });

        Button bCreate = (Button) popupView.findViewById(R.id.bCreateProject);
        bCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = etEdit.getText().toString();

                if (!name.equals(projects[currentProjectIndex].getName()) && !SBProjectsManager.getManager().renameProject(currentProjectIndex, name)){
                    showText("Project with that name already exist");
                }
                else{
                    window.dismiss();
                }
            }
        });
        return window;
    }

    public void showText(String text){
        Toast.makeText(fragment.getContext(), text, Toast.LENGTH_SHORT).show();
    }
}