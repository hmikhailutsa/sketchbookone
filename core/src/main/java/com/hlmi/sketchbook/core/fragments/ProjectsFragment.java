package com.hlmi.sketchbook.core.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.glar.tsat.fntools.TSFragment;
import com.hlmi.sketchbook.core.R;
import com.hlmi.sketchbook.core.activities.DrawingActivity;
import com.hlmi.sketchbook.core.adapters.SBProjectAdapter;
import com.hlmi.sketchbook.managers.SBProjectsManager;
import com.hlmi.sketchbook.models.SBBackgroundType;
import com.hlmi.sketchbook.models.SBProject;

public class ProjectsFragment extends TSFragment implements SBProjectsManager.SBProjectsManagerListener {


    private PopupWindow popUp;
    private PopupCreateViewHolder createViewHolder;
    private Type type;

    public enum Type { PROJECTS, TUTORIALS}


    private class PopupCreateViewHolder{
        public EditText etWidth;
        public EditText etName;
        public EditText etHeight;
        public Spinner sBackgroundType;
    }

    public ProjectsFragment() { type = Type.PROJECTS;}

    public ProjectsFragment(Type type){
        super();
        this.type = type;
    }

    @Override
    protected int getLayoutResourceID() {
        return R.layout.fragment_projects;
    }

    @Override
    public void onResume() {
        super.onResume();
        reloadProjects();
    }

    @Override
    protected void initializeFragmentView(final View parentView, Bundle savedInstanceState) {
        super.initializeFragmentView(parentView, savedInstanceState);
        createProjectPopup();
        SBProjectsManager.getManager().loadProjects();
        SBProjectsManager.getManager().loadTutorials(getContext());
        SBProjectsManager.getManager().addProjectsManagerListeners(this);
        reloadProjects();
        FloatingActionButton fabCreate = (FloatingActionButton) parentView.findViewById(R.id.fabCreate);
        boolean canCreate = type == Type.PROJECTS;
        fabCreate.setVisibility(canCreate ? View.VISIBLE : View.GONE);
        if (canCreate) {
            fabCreate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popUp.showAtLocation(getParentView(), Gravity.CENTER, 0, 0);
                }
            });
        }
    }

    private void reloadProjects(){
        GridView gvProjects = (GridView) getParentView().findViewById(R.id.gvProjects);
        ListAdapter adapter;
        switch (type){
            default:
            case PROJECTS:
                adapter = new SBProjectAdapter(this, SBProjectsManager.getManager().getProjects(), true);
                break;
            case TUTORIALS:
                adapter = new SBProjectAdapter(this, SBProjectsManager.getManager().getTuroials(), false);
                break;
        }
        gvProjects.setAdapter(adapter);
        gvProjects.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                openProject(i);
            }
        });
    }

    @Override
    public void onProjectRemoved(int index, SBProject project) {
        reloadProjects();
    }

    @Override
    public void onProjectRenamed(int index, String oldName, SBProject project) {
        reloadProjects();
    }

    private void createProjectPopup() {
        final View popupView = View.inflate(getContext(), R.layout.popup_create_project, null);
        popUp = new PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true);
        createViewHolder = new PopupCreateViewHolder();
        createViewHolder.sBackgroundType = (Spinner) popupView.findViewById(R.id.sBackground);
        createViewHolder.etWidth = (EditText) popupView.findViewById(R.id.etWidth);
        createViewHolder.etHeight = (EditText) popupView.findViewById(R.id.etHeight);
        createViewHolder.etName = (EditText) popupView.findViewById(R.id.etProjectName);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.background_contents_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        createViewHolder.sBackgroundType.setAdapter(adapter);

        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                closeProjectPopup();
                return true;
            }
        });

        final LinearLayout llPopUp = (LinearLayout) popupView.findViewById(R.id.llPopUp);
        llPopUp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        Button bCancel = (Button) popupView.findViewById(R.id.bCancel);
        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeProjectPopup();
            }
        });

        Button bCreate = (Button) popupView.findViewById(R.id.bCreateProject);
        bCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = createViewHolder.etName.getText().toString();
                String widthStr = createViewHolder.etWidth.getText().toString();
                String heightStr = createViewHolder.etHeight.getText().toString();

                if (name.isEmpty())
                    showText("Empty name");
                else if (widthStr.isEmpty())
                    showText("Empty width");
                else if (heightStr.isEmpty())
                    showText("Empty height");
                else if (SBProjectsManager.getManager().hasProjectNamed(name))
                    showText("Project already exist");
                else {
                    int width = Integer.parseInt(widthStr);
                    int height = Integer.parseInt(heightStr);
                    if (width <=0 || height <=0){
                        showText("Size must be greater than 0");
                        return;
                    }
                    else if (!checkMemory(width, height)) return;

                    SBBackgroundType type;
                    switch (createViewHolder.sBackgroundType.getSelectedItemPosition()) {
                        default:
                        case 0:
                            type = SBBackgroundType.WHITE;
                            break;
                        case 1:
                            type = SBBackgroundType.TRANSPARENT;
                            break;
                    }
                    openProject(SBProjectsManager.getManager().createProject(name, width, height, type));
                    reloadProjects();
                    closeProjectPopup();

                }


            }
        });
    }
    
    private void openProject(SBProject project){
        SBProjectsManager.getManager().setCurrentProject(project);
        getTSActivity().startActivity(DrawingActivity.class);
    }

    private void openProject(int index){
        SBProject p;
        switch (type) {
            default:
            case PROJECTS: p = SBProjectsManager.getManager().setCurrentProject(index); break;
            case TUTORIALS: p = SBProjectsManager.getManager().setCurrentTutorial(index); break;
        }
        if (checkMemory(p.getWidth(), p.getHeight()))
            getTSActivity().startActivity(DrawingActivity.class);
    }

    private boolean checkMemory(int w, int h){
        try {
            Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888).recycle();
        }
        catch (OutOfMemoryError e){
            showText("Not enough RAM memory");
            return false;
        }
        return true;
    }

    private void closeProjectPopup(){
        popUp.dismiss();
        createViewHolder.etHeight.setText(null);
        createViewHolder.etWidth.setText(null);
        createViewHolder.etName.setText(null);
        createViewHolder.sBackgroundType.setSelection(0);
    }

    public void showText(String text){
        Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
    }


}
