package com.hlmi.sketchbook.core.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import com.glar.tsat.fntools.TSFragment;
import com.hlmi.sketchbook.core.R;
import com.hlmi.sketchbook.core.adapters.SBBooksAdapter;
import com.hlmi.sketchbook.managers.SBBooksManager;
import com.hlmi.sketchbook.models.SBBook;
import com.hlmi.sketchbook.models.SBBook.SBBookCategory;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView;

import java.util.ArrayList;

public class BooksFragment extends TSFragment {

    @Override
    protected int getLayoutResourceID() {
        return R.layout.fragment_books;
    }

    @Override
    public void onResume() {
        super.onResume();
        reloadBooks();
    }

    @Override
    protected void initializeFragmentView(final View parentView, Bundle savedInstanceState) {
        super.initializeFragmentView(parentView, savedInstanceState);
        reloadBooks();
    }

    private void reloadBooks(){
        StickyGridHeadersGridView gvProjects = (StickyGridHeadersGridView) getParentView().findViewById(R.id.gvProjects);
        gvProjects.setAdapter(new SBBooksAdapter(this));
        gvProjects.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                openProject(i);
            }
        });
    }

    private void openProject(int index){
        SBBookCategory category = SBBookCategory.Basics;
        ArrayList<SBBook> books = SBBooksManager.getManager().getBooks(category);
        if (index >= books.size()) {
            index -= books.size();
            category = SBBookCategory.Techniques;
            books = SBBooksManager.getManager().getBooks(category);
            if (index >= books.size()) {
                index -= books.size();
                category = SBBookCategory.History;
            }
        }
        SBBooksManager.getManager().openBook(getContext(), category, index);
    }
}
