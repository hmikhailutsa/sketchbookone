package com.hlmi.sketchbook.core.activities;

import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.ColorInt;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.PopupWindowCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.*;
import com.chiralcode.colorpicker.ColorPickerDialog;
import com.glar.tsat.fntools.TSActivity;
import com.hlmi.sketchbook.core.R;
import com.hlmi.sketchbook.core.adapters.SBToolsAdapter;
import com.hlmi.sketchbook.core.adapters.SBTutorialStepsAdapter;
import com.hlmi.sketchbook.managers.SBActionManager;
import com.hlmi.sketchbook.managers.SBPaintingManager;
import com.hlmi.sketchbook.managers.SBProjectsManager;
import com.hlmi.sketchbook.models.SBProject;
import com.hlmi.sketchbook.models.SBTutorialStep;
import com.hlmi.sketchbook.models.tools.SBTool;
import com.hlmi.sketchbook.rendering.SBRenderingLayer;
import com.jess.TwoWayGridView;

import java.util.ArrayList;
import java.util.List;

public class DrawingActivity extends TSActivity implements SBPaintingManager.SBPaintingManagerListener {



    private class DrawingHeaderHolder{

        public RelativeLayout toolbar;
        public ImageButton ibBack;
        public TextView tvTitle;
        public ImageButton ibBrush;
        public ImageButton ibColorPicker;
        public ImageButton ibUndo;
        public ImageButton ibRedo;
        public ImageButton ibExport;
    }

    private class ToolsPopupHolder {
        public SeekBar sbSize;
        public EditText etSize;
    }

    private class TutorialOverlayHolder{
        public Button back;
        public Button next;

        public RelativeLayout footer;
        public FrameLayout header;

        public TwoWayGridView gvSteps;
        public BaseAdapter adapter;
    }

    private DrawingHeaderHolder headerHolder;
    private ToolsPopupHolder toolsPopupHolder;
    private TutorialOverlayHolder tutorialOverlayHolder;

    private ArrayList<SBTutorialStep> tutorialSteps;
    private int currentTutorialStep;

    @Override
    protected int getContainerID() {
        return 0;
    }

    @Override
    protected int getLayoutResourceID() {
        return R.layout.activity_drawing;
    }

    @Override
    protected void onTSActivityCreated(final View activityView) {
        FrameLayout holder = (FrameLayout) findViewById(R.id.flSurfaceHolder);
        SBPaintingManager.getManager().attachSurface(holder);
        SBProject project = SBProjectsManager.getManager().getCurrentProject();
        SBPaintingManager.getManager().loadProject(project);
        SBPaintingManager.getManager().addProjectsManagerListeners(this);
        headerHolder = new DrawingHeaderHolder();
        headerHolder.toolbar = (RelativeLayout) findViewById(R.id.rlToolbar);
        headerHolder.tvTitle = (TextView) findViewById(R.id.tvProjectTitle);
        headerHolder.tvTitle.setText(SBProjectsManager.getManager().getCurrentProject().getName());

        headerHolder.ibBack = (ImageButton) findViewById(R.id.ibBackToMain);
        headerHolder.ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeProject();
                finish();
            }
        });

        headerHolder.ibUndo = (ImageButton) findViewById(R.id.ibUndo);
        headerHolder.ibUndo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SBActionManager manager = SBActionManager.getManager();
                manager.rollbackLastAction();
                setButtonEnabled(manager.isRollbackAvailable(), headerHolder.ibUndo);
                setButtonEnabled(manager.isExecuteAvailable(), headerHolder.ibRedo);
            }
        });

        headerHolder.ibRedo = (ImageButton) findViewById(R.id.ibRedo);
        headerHolder.ibRedo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SBActionManager manager = SBActionManager.getManager();
                manager.executeLastAction();
                setButtonEnabled(manager.isRollbackAvailable(), headerHolder.ibUndo);
                setButtonEnabled(manager.isExecuteAvailable(), headerHolder.ibRedo);
            }
        });
        setButtonEnabled(false, headerHolder.ibUndo);
        setButtonEnabled(false, headerHolder.ibRedo);

        headerHolder.ibExport = (ImageButton) findViewById(R.id.ibExport);
        headerHolder.ibExport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SBPaintingManager.getManager().exportProject();
            }
        });

        headerHolder.ibColorPicker = (ImageButton) findViewById(R.id.ibColor);

        headerHolder.ibColorPicker.setColorFilter(new LightingColorFilter(Color.WHITE, SBPaintingManager.getManager().getTool().getColor()));
        headerHolder.ibColorPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ColorPickerDialog colorPickerDialog = new ColorPickerDialog(DrawingActivity.this, SBPaintingManager.getManager().getTool().getColor(), new ColorPickerDialog.OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int color) {
                        setToolColor(color);
                    }
                });
                colorPickerDialog.show();
            }
        });

        headerHolder.ibBrush = (ImageButton) findViewById(R.id.ibPen);
        headerHolder.ibBrush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupWindowCompat.showAsDropDown(createToolsPopup(),view, 0, 0, Gravity.CENTER);
            }
        });
        setTool(0);

        tutorialSteps = new ArrayList<>();
        createTutorialSteps();
        tutorialOverlayHolder = new TutorialOverlayHolder();
        tutorialOverlayHolder.header = (FrameLayout) findViewById(R.id.flTutorialSteps);
        tutorialOverlayHolder.footer = (RelativeLayout) findViewById(R.id.rlTutorialNavigartion);
        tutorialOverlayHolder.back = (Button) findViewById(R.id.bTutorialBack);
        tutorialOverlayHolder.next = (Button) findViewById(R.id.bTutorialNext);
        tutorialOverlayHolder.gvSteps = (TwoWayGridView) findViewById(R.id.gvTutorialSteps);

        tutorialOverlayHolder.header.setVisibility(!project.isEditable() ? View.VISIBLE : View.GONE);
        tutorialOverlayHolder.footer.setVisibility(!project.isEditable() ? View.VISIBLE : View.GONE);

        tutorialOverlayHolder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectPrevStep();
                view.setEnabled(currentTutorialStep > 0);
                tutorialOverlayHolder.next.setEnabled(true);
            }
        });
        tutorialOverlayHolder.next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectNextStep();
                view.setEnabled(currentTutorialStep < tutorialSteps.size() - 1);
                tutorialOverlayHolder.back.setEnabled(true);
            }
        });

        tutorialOverlayHolder.adapter = new SBTutorialStepsAdapter(this, tutorialSteps);
        selectCurrentTutorialStep(currentTutorialStep);
        tutorialOverlayHolder.gvSteps.setAdapter(tutorialOverlayHolder.adapter);
    }

    @Override
    public void onPaintingComplete() {
        setButtonEnabled(SBActionManager.getManager().isRollbackAvailable(), headerHolder.ibUndo);
        setButtonEnabled(SBActionManager.getManager().isExecuteAvailable(), headerHolder.ibRedo);
    }

    private void setButtonEnabled(boolean enabled, ImageButton btn){
        btn.setAlpha(enabled ? 1 : 0.5f);
        btn.setEnabled(enabled);
    }

    private void closeProject(){
        if (SBProjectsManager.getManager().getCurrentProject().isEditable()) {
            SBPaintingManager.getManager().saveProject();
            SBProjectsManager.getManager().saveProject();
        }
        SBActionManager.getManager().clear();

    }

     private PopupWindow createToolsPopup(){
         final View popupView = View.inflate(this, R.layout.popup_select_tool, null);

         final PopupWindow popUp = new PopupWindow(popupView, (int)getResources().getDimension(R.dimen.popup_tools_width), (int)getResources().getDimension(R.dimen.popup_tools_height), true);
         popUp.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, android.R.color.transparent)));
         popUp.setOutsideTouchable(true);
         GridView grid = (GridView) popupView.findViewById(R.id.gvTools);
         grid.setAdapter(new SBToolsAdapter(this, SBPaintingManager.getManager().getAvailableTools()));
         grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
             @Override
             public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                 popUp.dismiss();
                 setTool(i);
             }
         });
         toolsPopupHolder = new ToolsPopupHolder();
         toolsPopupHolder.sbSize = (SeekBar) popupView.findViewById(R.id.sbToolSize);
         toolsPopupHolder.etSize = (EditText) popupView.findViewById(R.id.etToolSize);

         setToolSize((int) SBPaintingManager.getManager().getTool().getSize());

         toolsPopupHolder.sbSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
             @Override
             public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                 if (!b) return;
                 setToolSize(i);
             }

             @Override public void onStartTrackingTouch(SeekBar seekBar) { }
             @Override public void onStopTrackingTouch(SeekBar seekBar) { }
         });

         toolsPopupHolder.etSize.addTextChangedListener(new TextWatcher() {
             @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
             @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
             @Override
             public void afterTextChanged(Editable editable) {
                 if (!editable.toString().equals(toolsPopupHolder.etSize.getText().toString()))
                    setToolSize(editable.toString());
             }

         });

         return popUp;
     }


    private void createTutorialSteps(){
        SBProject project = SBProjectsManager.getManager().getCurrentProject();
        List<SBRenderingLayer> layers = project.getLayers();
        for (int i = 1; i < layers.size(); i++) {
            if (!layers.get(i).isEditable() && layers.get(i).getType() == SBRenderingLayer.SBRenderingLayerType.CUSTOM)
                tutorialSteps.add(new SBTutorialStep(layers.get(i).getName(), false));
        }
        currentTutorialStep = 0;
    }

    private boolean selectNextStep(){
        if (currentTutorialStep < tutorialSteps.size() - 1) {
            selectCurrentTutorialStep(currentTutorialStep + 1);
            return true;
        }
        return false;
    }

    private boolean selectPrevStep(){
        if (currentTutorialStep > 0) {
            selectCurrentTutorialStep(currentTutorialStep - 1);
            return true;
        }
        return false;
    }

    private void selectCurrentTutorialStep(int index){
        if (index < 0 || index >= tutorialSteps.size()) return;
        tutorialSteps.get(currentTutorialStep).setCurrent(false);
        tutorialSteps.get(index).setCurrent(true);
        tutorialOverlayHolder.adapter.notifyDataSetChanged();
        SBPaintingManager.getManager().getProject().getLayers().get(currentTutorialStep + 1).setEnabled(false);
        SBPaintingManager.getManager().getProject().getLayers().get(index + 1).setEnabled(true);
        currentTutorialStep = index;
    }

    private void setTool(int index) {
        SBTool tool = SBPaintingManager.getManager().selectTool(index);
        setToolColor(tool.getColor());
        setToolSize((int) tool.getSize());
        headerHolder.ibBrush.setImageResource(tool.getIcon());
        headerHolder.ibBrush.setColorFilter(new LightingColorFilter(Color.BLACK, Color.WHITE));
    }

    private void setToolSize(int size){
        if (toolsPopupHolder == null) return;
        size = (size <= 0 ? 1 : size);
        size = (size > toolsPopupHolder.sbSize.getMax() ? toolsPopupHolder.sbSize.getMax() : size);
        toolsPopupHolder.sbSize.setProgress(size);
        SBPaintingManager.getManager().getTool().setSize(size);
        toolsPopupHolder.etSize.setText(String.valueOf(size));
    }

    private void setToolSize(String sizeString){
        setToolSize(Integer.valueOf(sizeString));
    }

    private void setToolColor(@ColorInt int color){
        SBPaintingManager.getManager().getTool().setColor(color);
        headerHolder.ibColorPicker.setColorFilter(new LightingColorFilter(Color.WHITE, color));
    }

    @Override
    public void onBackPressed() {
        closeProject();
        super.onBackPressed();
    }

    @Override
    public <T extends Fragment> Class<T> getInitialFragmentClass() {
        return null;
    }
}
