package com.hlmi.sketchbook.models.tools;

import android.content.Context;
import android.graphics.*;
import com.hlmi.sketchbook.core.R;

public class SBEraserPatternTool extends SBPatternTool {
    public SBEraserPatternTool(Context context) {
        super((context == null ? null : BitmapFactory.decodeResource(context.getResources(), R.drawable.eraser)), R.drawable.ic_eraser);
    }

    protected SBEraserPatternTool() {}

    @Override
    protected Paint createPaint() {
        Paint eraserPaint = new Paint();
        eraserPaint.setStrokeWidth(30);
        eraserPaint.setColor(Color.TRANSPARENT);
        eraserPaint.setStyle(Paint.Style.STROKE);
        eraserPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        eraserPaint.setAntiAlias(true);
        return eraserPaint;
    }

    @Override
    public void setColor(int color) {
        super.setColor(color);
        toolPaint.setColorFilter(new PorterDuffColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR));
    }
}
