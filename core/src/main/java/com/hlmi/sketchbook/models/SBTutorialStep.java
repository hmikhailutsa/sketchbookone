package com.hlmi.sketchbook.models;

public class SBTutorialStep {
    private String name;
    private boolean current;

    public SBTutorialStep(String name, boolean current) {
        this.name = name;
        this.current = current;
    }

    public String getName() {
        return name;
    }

    public boolean isCurrent() {
        return current;
    }

    public void setCurrent(boolean current) {
        this.current = current;
    }
}
