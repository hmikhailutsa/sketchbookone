package com.hlmi.sketchbook.models.tools;

import android.content.Context;
import android.graphics.BitmapFactory;
import com.hlmi.sketchbook.core.R;

public class SBBrushPatternTool extends SBPatternTool {
    public SBBrushPatternTool(Context context) {
        super((context == null ? null : BitmapFactory.decodeResource(context.getResources(), R.drawable.brush)), R.drawable.ic_brush);
    }

    protected SBBrushPatternTool() {}
}
