package com.hlmi.sketchbook.models.tools;

import android.content.Context;
import android.graphics.BitmapFactory;
import com.hlmi.sketchbook.core.R;

public class SBHardBrushPatternTool extends SBPatternTool {
    public SBHardBrushPatternTool(Context context) {
        super((context == null ? null : BitmapFactory.decodeResource(context.getResources(), R.drawable.hard_brush)), R.drawable.ic_hard_brush);
    }

    protected SBHardBrushPatternTool() {}
}
