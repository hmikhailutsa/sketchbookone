package com.hlmi.sketchbook.models.tools;

import android.graphics.*;
import android.support.annotation.DrawableRes;
import com.hlmi.sketchbook.helpers.SerializableBitmap;

public abstract class SBPatternTool extends SBTool {

    protected SerializableBitmap pattern;
    protected SerializableBitmap scaledPattern;

    protected SBPatternTool() {}

    protected SBPatternTool(Bitmap pattern, @DrawableRes int icon) {
        super("Pattern Brush", "Pattern Brush", icon);
        this.pattern = new SerializableBitmap(pattern);
        this.scaledPattern = new SerializableBitmap(Bitmap.createScaledBitmap(pattern, (int) getSize(), (int) getSize(), true));
        setColor(Color.WHITE);
    }

    @Override
    protected Paint createPaint() {
        Paint paint = new Paint();
        paint.setStrokeWidth(50);
        paint.setStyle(Paint.Style.STROKE);
        return paint;
    }

    public Bitmap getPattern() {
        return pattern.getBitmap();
    }

    public Bitmap getScaledPattern() {
        return scaledPattern.getBitmap();
    }

    @Override
    public void setSize(float size) {
        super.setSize(size);
        this.scaledPattern.setBitmap(Bitmap.createScaledBitmap(pattern.getBitmap(), (int) toolPaint.getStrokeWidth(), (int) toolPaint.getStrokeWidth(), true));
    }

    @Override
    public void setColor(int color) {
        super.setColor(color);
        toolPaint.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN));
    }

    @Override
    public SBPatternTool clone() {
        SBPatternTool tool = (SBPatternTool) super.clone();
        tool.pattern = this.pattern;
        tool.scaledPattern = this.scaledPattern;
        return tool;
    }
}
