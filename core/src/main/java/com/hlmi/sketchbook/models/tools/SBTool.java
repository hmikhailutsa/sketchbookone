package com.hlmi.sketchbook.models.tools;

import android.graphics.Paint;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;

import java.io.IOException;
import java.io.Serializable;

public abstract class SBTool implements Cloneable, Serializable {

    protected String name;
    protected String descr;
    transient protected Paint toolPaint;
    protected @DrawableRes int icon;

    protected SBTool(){} // for cloning

    protected SBTool(String name, String descr, @DrawableRes int icon) {
        this.name = name;
        this.descr = descr;
        this.icon = icon;
        this.toolPaint = createPaint();
    }

    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        toolPaint = createPaint();
    }

    protected abstract Paint createPaint();

    public String getName() {
        return name;
    }

    public String getDescr() {
        return descr;
    }

    public Paint getPaint() {
        return toolPaint;
    }

    @DrawableRes
    public int getIcon() {
        return icon;
    }

    public void setSize(float size){
        toolPaint.setStrokeWidth(size);
    }

    public void setColor(@ColorInt int color){
        toolPaint.setColor(color);
    }

    public int getColor() {
        return toolPaint.getColor();
    }

    public float getSize() {
        return toolPaint.getStrokeWidth();
    }

    @Override
    public SBTool clone(){
        SBTool tool = null;
        try {
             tool = this.getClass().newInstance();
            tool.name = this.name;
            tool.descr = this.descr;
            tool.icon = this.icon;
            tool.toolPaint = new Paint(this.toolPaint);
        } catch (InstantiationException e){
            e.printStackTrace();
        }   catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return tool;
    }
}
