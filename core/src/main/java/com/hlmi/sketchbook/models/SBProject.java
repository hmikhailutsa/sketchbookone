package com.hlmi.sketchbook.models;


import android.graphics.Bitmap;
import com.hlmi.sketchbook.helpers.SerializableBitmap;
import com.hlmi.sketchbook.rendering.SBRenderingLayer;

import java.io.Serializable;
import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;

public class SBProject implements Serializable{

    public static final String EXTENSION_PROJ = ".sboproj";
    public static final String EXTENSION_PNG = ".png";

    private String name;
    private int width;
    private int height;
    private SBBackgroundType backgroundType;
    private SerializableBitmap thumbnail;

    private boolean isEditable;

    private CopyOnWriteArrayList<SBRenderingLayer> layers;

    public SBProject(String name, int width, int height) {
        this(name, width, height, true);
    }

    public SBProject(String name, int width, int height, boolean isEditable) {
        this(name, width, height, SBBackgroundType.WHITE, isEditable);
    }

    public SBProject(String name, int width, int height, SBBackgroundType backgroundType) {
        this(name, width, height, backgroundType, true);
    }

    public SBProject(String name, int width, int height, SBBackgroundType backgroundType, boolean isEditable) {
        this(name,width,height,backgroundType, Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_8888), isEditable);
    }

    public SBProject(String name, int width, int height, SBBackgroundType backgroundType, Bitmap thumbnail) {
        this(name, width, height, backgroundType, thumbnail, true);
    }

    public SBProject(String name, int width, int height, SBBackgroundType backgroundType, Bitmap thumbnail, boolean isEditable) {
        this.name = name;
        this.width = width;
        this.height = height;
        this.backgroundType = backgroundType;
        this.thumbnail = new SerializableBitmap(thumbnail);
        this.layers = new CopyOnWriteArrayList<>();
        setEditable(isEditable);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEditable() {
        return isEditable;
    }

    private void setEditable(boolean editable) {
        isEditable = editable;
        for (SBRenderingLayer layer : layers) {
            layer.setEditable(isEditable);
        }
    }

    public Bitmap getThumbnail() {
        return thumbnail.getBitmap();
    }

    public void setThumbnail(Bitmap thumbnail) {
        this.thumbnail.getBitmap().recycle();
        this.thumbnail = new SerializableBitmap(thumbnail);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public SBBackgroundType getBackgroundType() {
        return backgroundType;
    }

    public CopyOnWriteArrayList<SBRenderingLayer> getLayers() {
        return layers;
    }

    public void addLayer(SBRenderingLayer layer) {
        addLayer(layer, isEditable);
    }

    public void addLayer(SBRenderingLayer layer, boolean isEditable){
        this.layers.add(layer);
        layer.setEditable(isEditable);
    }

    public void addLayers(Collection<SBRenderingLayer> layers) {
        addLayers(layers, isEditable);
    }

    public void addLayers(Collection<SBRenderingLayer> layers, boolean isEditable){
        this.layers.addAll(layers);
        setEditable(isEditable);
    }

    public void setLayers(CopyOnWriteArrayList<SBRenderingLayer> layers, boolean inheritEditable) {
        this.layers = layers;
        if (inheritEditable) setEditable(isEditable);
    }


}
