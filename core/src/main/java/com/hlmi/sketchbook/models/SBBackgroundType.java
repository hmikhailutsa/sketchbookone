package com.hlmi.sketchbook.models;

public enum SBBackgroundType {
    UNKNOWN,
    WHITE,
    TRANSPARENT
}
