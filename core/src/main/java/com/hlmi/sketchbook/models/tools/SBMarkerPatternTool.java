package com.hlmi.sketchbook.models.tools;

import android.content.Context;
import android.graphics.BitmapFactory;
import com.hlmi.sketchbook.core.R;

public class SBMarkerPatternTool extends SBPatternTool {
    public SBMarkerPatternTool(Context context) {
        super((context == null ? null : BitmapFactory.decodeResource(context.getResources(), R.drawable.marker)), R.drawable.ic_marker);
    }

    protected SBMarkerPatternTool() {}
}
