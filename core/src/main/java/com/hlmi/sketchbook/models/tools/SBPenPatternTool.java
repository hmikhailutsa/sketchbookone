package com.hlmi.sketchbook.models.tools;

import android.content.Context;
import android.graphics.BitmapFactory;
import com.hlmi.sketchbook.core.R;

public class SBPenPatternTool extends SBPatternTool {
    public SBPenPatternTool(Context context) {
        super((context == null ? null : BitmapFactory.decodeResource(context.getResources(), R.drawable.pen)), R.drawable.ic_pen);
    }

    protected SBPenPatternTool() {}
}
