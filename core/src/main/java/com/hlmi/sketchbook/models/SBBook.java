package com.hlmi.sketchbook.models;

import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;

public class SBBook {



    public enum SBBookCategory {
        Basics, Techniques, History
    }

    private String name;
    private String authors;
    private int drawable;
    private String filename;
    private SBBookCategory category;

    private int accentColor;

    private int textColor;

    public SBBook(String name, String authors, @DrawableRes int drawable, String filename, SBBookCategory category, @ColorRes int accentColor, @ColorRes int textColor) {
        this.name = name;
        this.authors = authors;
        this.drawable = drawable;
        this.filename = filename;
        this.category = category;
        this.accentColor = accentColor;
        this.textColor = textColor;
    }

    public String getName() {
        return name;
    }

    public String getAuthors() {
        return authors;
    }

    public int getDrawable() {
        return drawable;
    }

    public String getFilename() {
        return filename;
    }

    public SBBookCategory getCategory() {
        return category;
    }

    public int getTextColor() {
        return textColor;
    }

    public int getAccentColor() {
        return accentColor;
    }
}
