package com.hlmi.sketchbook.models.tools;

import android.graphics.Color;
import android.graphics.Paint;
import com.hlmi.sketchbook.core.R;

public class SBBrushTool extends SBTool{

    @Override
    protected Paint createPaint(){
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(40.0f);
        paint.setAntiAlias(true);
        return paint;
    }

    public SBBrushTool() {
        super("Brush", "Simple brush", R.drawable.ic_brush);
    }
}
