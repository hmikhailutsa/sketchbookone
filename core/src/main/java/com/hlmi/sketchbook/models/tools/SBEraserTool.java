package com.hlmi.sketchbook.models.tools;

import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import com.hlmi.sketchbook.core.R;

public class SBEraserTool extends SBTool {

    public SBEraserTool() {
        super("Eraser", "Clears canvas", R.drawable.eraser);
    }

    @Override
    protected Paint createPaint() {
        Paint eraserPaint = new Paint();
        eraserPaint.setStrokeWidth(30);
        eraserPaint.setStyle(Paint.Style.STROKE);
        eraserPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        eraserPaint.setAntiAlias(true);
        return eraserPaint;
    }
}
