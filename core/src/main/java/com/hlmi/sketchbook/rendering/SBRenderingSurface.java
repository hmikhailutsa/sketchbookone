package com.hlmi.sketchbook.rendering;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public class SBRenderingSurface extends SurfaceView implements Runnable, SurfaceHolder.Callback {

    private Thread renderingThread;
    private SurfaceHolder surfaceHolder;
    private CopyOnWriteArrayList<SBRenderingLayer> renderableData;
    private Paint unitPaint;
    private boolean isRunning; // thread is running now.

    private int width;
    private int height;

    public SBRenderingSurface(Context context) {
        super(context);
        surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);
        renderableData = new CopyOnWriteArrayList<>();
        unitPaint = new Paint(Paint.FILTER_BITMAP_FLAG | Paint.DITHER_FLAG | Paint.ANTI_ALIAS_FLAG);
    }

    @Override
    public void run() {
        while (isRunning) {
            if (!surfaceHolder.getSurface().isValid()) {
                continue;
            }
            Canvas canvas = surfaceHolder.lockCanvas();
            if (canvas == null || renderableData == null)
                continue;

            width = canvas.getWidth();
            height = canvas.getHeight();
            Iterator<SBRenderingLayer> iterator = renderableData.iterator();
            SBRenderingLayer item;
            while (iterator.hasNext()) {
                item = iterator.next();
                if (item.isEnabled() && !item.isHidden())
                   item.renderLayer(canvas);
            }
            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    public void setLayers(CopyOnWriteArrayList<SBRenderingLayer> layers) {
        this.renderableData = layers;
    }

    public int getRenderingHeight() {
        return height;
    }

    public int getRenderingWidth() {
        return width;
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        Log.d("SURFACE CREATED","");
        isRunning = true;
        renderingThread = new Thread(this);
        renderingThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        int tmp = width;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        isRunning = false;
        Log.d("SURFACE DESTOYED","");
        while (true) {
            try {
                renderingThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            break;
        }
        renderingThread = null;
    }
}
