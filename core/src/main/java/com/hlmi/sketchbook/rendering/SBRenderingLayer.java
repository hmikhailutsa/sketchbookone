package com.hlmi.sketchbook.rendering;

import android.graphics.Canvas;
import android.util.Log;
import com.hlmi.sketchbook.actions.DrawAction;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class SBRenderingLayer implements Serializable{



    public enum SBRenderingLayerType{
        BACKGROUND, OVERLAY, CUSTOM
    }

    private String name;

    private CopyOnWriteArrayList<DrawAction> drawActions;

    private boolean isEdditable;
    private SBRenderingLayerType type;

    private boolean hidden;  // Flag which is used to indicate that layer should be rendered (user flag)
    private boolean enabled; // Flag which is used to indicate that layer should be rendered (system flag)

    public SBRenderingLayer(String name) {
        this(name, SBRenderingLayerType.CUSTOM);
    }

    public SBRenderingLayer(String name, SBRenderingLayerType type) {
        this.name = name;
        this.isEdditable = true;
        this.hidden = false;
        this.enabled = true;
        this.type = type;
        drawActions = new CopyOnWriteArrayList<>();
        clear();
    }

    public String getName() {
        return name;
    }

    public boolean isEditable() {
        return isEdditable;
    }

    public void setEditable(boolean editable) {
        isEdditable = editable;
    }

    /** Clears layer's Canvas. */
    public void clear(){
        drawActions.clear();
    }

    public void addAction(DrawAction action){
        drawActions.add(action);
    }
    public void addActions(Collection<DrawAction> actions){
        this.drawActions.addAll(actions);
    }

    public void removeAction(DrawAction action){
        drawActions.remove(action);
    }
    public void removeActions(Collection<DrawAction> actions) {
        this.drawActions.removeAll(actions);
    }

    public void renderLayer(Canvas c){
        if (!isEnabled()){
            Log.d("SBLayer " + getName(), "Not enabled!");return;}
        for (DrawAction action : drawActions) {
            action.draw(c);
        }
    }
    public List<DrawAction> getActions(){ return (List<DrawAction>) drawActions.clone();}

    public SBRenderingLayerType getType() {
        return type;
    }

    public void setType(SBRenderingLayerType type) {
        this.type = type;
    }

    /** Gets enabled flag which is used to indicate that layer should be rendered (used by user).  */
    public boolean isHidden() {
        return hidden;
    }

    /** Sets enabled flag which is used to indicate that layer should be rendered (used by user). */
    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    /** Gets enabled flag which is used to indicate that layer should be rendered (used by system). */
    public boolean isEnabled() {
        return enabled;
    }

    /** Sets enabled flag which is used to indicate that layer should be rendered (used by system). */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
