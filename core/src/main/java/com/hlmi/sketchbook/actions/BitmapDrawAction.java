package com.hlmi.sketchbook.actions;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import com.hlmi.sketchbook.helpers.SBPoint;
import com.hlmi.sketchbook.helpers.SerializableBitmap;

public class BitmapDrawAction extends DrawAction {

    private SerializableBitmap bmp;
    private SBPoint position;

    public BitmapDrawAction(Bitmap bitmap, SBPoint position) {
        super("Draw bitmap on layer");
        this.bmp = new SerializableBitmap(bitmap);
        this.position = position;
    }

    @Override
    public void draw(Canvas c) {
        if (!bmp.getBitmap().isRecycled())
            c.drawBitmap(bmp.getBitmap(), position.x, position.y, null);
    }
}
