package com.hlmi.sketchbook.actions;

import java.io.Serializable;

public  abstract class DrawAction implements DrawCommand, Serializable {
    protected String description;

    public DrawAction(String description) {
        this.description = description;
    }

    public final String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Action {" +
                "description='" + description + '\'' +
                '}';
    }
}
