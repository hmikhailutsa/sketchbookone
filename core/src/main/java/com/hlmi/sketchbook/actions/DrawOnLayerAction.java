package com.hlmi.sketchbook.actions;

import com.hlmi.sketchbook.rendering.SBRenderingLayer;

import java.util.Arrays;
import java.util.Collection;

public class DrawOnLayerAction extends Action {


    private SBRenderingLayer layer;
    private Collection<DrawAction> drawActions;

    public DrawOnLayerAction(SBRenderingLayer layer, Collection<DrawAction> drawActions) {
        super(layer.getName());
        this.layer = layer;
        this.drawActions = drawActions;
    }
    public DrawOnLayerAction(SBRenderingLayer layer, DrawAction... drawActions) {
        this(layer, Arrays.asList(drawActions));
    }

    @Override
    public void execute() {
        layer.addActions(drawActions);
    }

    @Override
    public void rollback() {
        layer.removeActions(drawActions);
    }
}
