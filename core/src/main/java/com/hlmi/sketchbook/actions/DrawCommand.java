package com.hlmi.sketchbook.actions;

import android.graphics.Canvas;

public interface DrawCommand{
    void draw(Canvas c);
}
