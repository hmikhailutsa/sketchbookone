package com.hlmi.sketchbook.actions;

import android.graphics.*;
import com.hlmi.sketchbook.helpers.SerializableBitmap;

public class FillPatternDrawAction extends DrawAction {

    private SerializableBitmap pattern;

    public FillPatternDrawAction(Bitmap pattern) {
        super("Fill layer with pattern image");
        this.pattern = new SerializableBitmap(pattern);
    }

    @Override
    public void draw(Canvas target) {
        Paint p = new Paint();
        BitmapShader patternBMPshader = new BitmapShader(pattern.getBitmap(), Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        p.setShader(patternBMPshader);
        p.setStyle(Paint.Style.FILL);
        target.drawRect(0, 0, target.getWidth(), target.getHeight(), p);
    }


}
