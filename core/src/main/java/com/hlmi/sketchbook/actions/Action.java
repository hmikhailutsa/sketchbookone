package com.hlmi.sketchbook.actions;

public abstract class Action implements Command{
    protected String description;

    public Action(String description) {
        this.description = description;
    }

    public final String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Action {" +
                "description='" + description + '\'' +
                '}';
    }
}
