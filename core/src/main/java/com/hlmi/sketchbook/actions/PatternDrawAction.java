package com.hlmi.sketchbook.actions;

import android.graphics.Canvas;
import com.hlmi.sketchbook.helpers.SBPoint;
import com.hlmi.sketchbook.models.tools.SBPatternTool;

import java.util.HashSet;
import java.util.Set;

public class PatternDrawAction extends DrawAction {

    private Set<SBPoint> points;
    private SBPatternTool tool;

    private PatternDrawAction(SBPatternTool tool){
        super("Draw pattern on layer");
        this.tool = tool.clone();
    }

    public PatternDrawAction(Set<SBPoint> points, SBPatternTool tool) {
        this(tool);
        this.points = points;
    }

    public PatternDrawAction(SBPoint point, SBPatternTool tool) {
        this(tool);
        this.points = new HashSet<>();
        this.points.add(point);
    }

    @Override
    public void draw(Canvas c) {
        int w = tool.getScaledPattern().getWidth()/2;
        int h = tool.getScaledPattern().getHeight()/2;
        if (!tool.getScaledPattern().isRecycled()) {
            for (SBPoint pt : points)
                c.drawBitmap(tool.getScaledPattern(), pt.x - w, pt.y - h, tool.getPaint());
        }
    }
}
