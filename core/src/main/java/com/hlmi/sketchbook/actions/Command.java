package com.hlmi.sketchbook.actions;

public interface Command {

    public void execute();
    public void rollback();
}
