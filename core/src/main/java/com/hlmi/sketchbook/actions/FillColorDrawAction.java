package com.hlmi.sketchbook.actions;

import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.support.annotation.ColorInt;

public class FillColorDrawAction extends DrawAction {

    private @ColorInt int color;
    private PorterDuff.Mode mode;

    public FillColorDrawAction(@ColorInt int color) {
        this(color, null);
    }

    public FillColorDrawAction(@ColorInt int color, PorterDuff.Mode mode) {
        super("Fill layer with color");
        this.color = color;
        this.mode = mode;
    }

    @Override
    public void draw(Canvas target) {
        if (mode != null)
            target.drawColor(color, mode);
        else
            target.drawColor(color);
    }
}
