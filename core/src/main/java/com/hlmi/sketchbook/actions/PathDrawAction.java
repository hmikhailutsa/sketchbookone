package com.hlmi.sketchbook.actions;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

public class PathDrawAction extends DrawAction {

    private Path path;
    private Paint paint;

    public PathDrawAction(Path path, Paint paint) {
        super("Draw path on layer");
        this.path = new Path(path);
        this.paint = new Paint(paint);
    }

    @Override
    public void draw(Canvas c) {
        c.drawPath(path, paint);
    }
}
