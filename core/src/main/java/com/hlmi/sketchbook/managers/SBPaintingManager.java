package com.hlmi.sketchbook.managers;

import android.content.Context;
import android.graphics.*;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.hlmi.sketchbook.actions.*;
import com.hlmi.sketchbook.core.R;
import com.hlmi.sketchbook.helpers.SBPoint;
import com.hlmi.sketchbook.helpers.StorageManager;
import com.hlmi.sketchbook.models.SBBackgroundType;
import com.hlmi.sketchbook.models.SBProject;
import com.hlmi.sketchbook.models.tools.*;
import com.hlmi.sketchbook.rendering.SBRenderingLayer;
import com.hlmi.sketchbook.rendering.SBRenderingSurface;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;

public class SBPaintingManager implements View.OnTouchListener{


    private HashMap<String, SBTool> availableTools;

    private SBTool systemTool;
    private SBTool tool;
    private Path drawingPath;
    private Set<SBPoint> drawingPoints;



    private SBProject project;

    private SBRenderingLayer selectedLayer;
    private SBRenderingLayer tmpLayer;
    private SBRenderingLayer backgroundLayer;


    private SBRenderingSurface renderingSurface;
    private ViewGroup parent;
    private RelativeLayout container;

    private static SBPaintingManager manager;
    private ArrayList<SBPaintingManagerListener> paintingManagerListeners;

    public static SBPaintingManager getManager() {
        if (manager == null) manager = new SBPaintingManager();
        return manager;
    }

    private SBPaintingManager() {
        systemTool = new SBBrushTool(); paintingManagerListeners = new ArrayList<>();
    }

    private void initTools() {
        Context c = parent.getContext();
        availableTools = new HashMap<>();
        availableTools.put(SBPenPatternTool.class.getName(), new SBPenPatternTool(c));
        availableTools.put(SBBrushPatternTool.class.getName(), new SBBrushPatternTool(c));
        availableTools.put(SBHardBrushPatternTool.class.getName(), new SBHardBrushPatternTool(c));
        availableTools.put(SBMarkerPatternTool.class.getName(), new SBMarkerPatternTool(c));
        availableTools.put(SBEraserPatternTool.class.getName(), new SBEraserPatternTool(c));
        selectTool(SBBrushPatternTool.class);
    }

    public SBTool getTool() {
        return tool;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (selectedLayer == null || !selectedLayer.isEditable()) return true;
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        switch (motionEvent.getAction()){
            case MotionEvent.ACTION_DOWN:{
                if (tool instanceof SBPatternTool){
                    drawingPoints = new TreeSet<>();
                }
                else {
                    drawingPath = new Path();
                    drawingPath.moveTo(x, y);
                }
                if (tool instanceof SBEraserPatternTool) {
                    // Since eraser is a true cleaning tool we have to temporary disable selected layer (where cleaning performed)
                    // and copy its content to the tmpLayer where eraser will clear drawing
                    tmpLayer.addActions(selectedLayer.getActions());
                    selectedLayer.clear();
                    selectedLayer.setEnabled(false);
                }
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                if (tool instanceof SBPatternTool){
                    tmpLayer.addAction(new PatternDrawAction(new SBPoint(x, y), (SBPatternTool) tool));
                }
                else {
                    drawingPath.lineTo(x, y);
                    tmpLayer.addAction(new PathDrawAction(drawingPath, tool.getPaint()));
                }
                break;
            }
            case MotionEvent.ACTION_UP:{
                selectedLayer.setEnabled(true);
                SBActionManager.getManager().executeAction(new DrawOnLayerAction(selectedLayer, tmpLayer.getActions()));
                tmpLayer.clear();
                notifyPaint();
                break;
            }
            default:
            case MotionEvent.ACTION_CANCEL:{
                drawingPath = null;
                tmpLayer.clear();
                selectedLayer.setEnabled(true);
                break;
            }
        }


        return true;
    }

    public SBProject getProject() {
        return project;
    }

    public void loadProject(SBProject project){
        this.project = project;
        renderingSurface.getHolder().setFixedSize(project.getWidth(), project.getHeight());
        createSystemLayers();
        if (project.getLayers().size() == 1) {
            addLayer();
        }
        for (int i = 1; i < project.getLayers().size(); i++) {
            if (project.getLayers().get(i).isEditable()) {
                selectLayer(i);
                break;
            }
        }
        renderingSurface.setLayers(project.getLayers());

    }

    public void saveProject(){
        project.getLayers().remove(tmpLayer);

        float maxDim = Math.max(project.getWidth(), project.getHeight());
        final float thumbSize = 100;
        float scale = (maxDim <= thumbSize ? 1 : thumbSize / maxDim);
        int width = (int) (project.getWidth() * scale);
        int height = (int) (project.getHeight() * scale);
        Bitmap tmp = Bitmap.createBitmap(project.getWidth(), project.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(tmp);
        for (SBRenderingLayer layer : project.getLayers()) {
            layer.renderLayer(c);
        }
        Bitmap thumb = Bitmap.createScaledBitmap(tmp, width, height, true);
        project.setThumbnail(thumb);
    }

    public void exportProject(){
        Bitmap bitmap = Bitmap.createBitmap(project.getWidth(), project.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bitmap);
        for (SBRenderingLayer layer : project.getLayers()) {
            layer.renderLayer(c);
        }
        try {
            OutputStream stream = new FileOutputStream(new File(StorageManager.openExternalDirectory(SBProjectsManager.EXPORT_DIR), SBProjectsManager.getManager().getProjectImageFilename(project)));
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            stream.close();
            bitmap.recycle();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void createSystemLayers(){
        tmpLayer = new SBRenderingLayer("System Layer", SBRenderingLayer.SBRenderingLayerType.OVERLAY);
        CopyOnWriteArrayList<SBRenderingLayer> layers = project.getLayers();

        if (layers.size() == 0 || layers.get(0).getType() != SBRenderingLayer.SBRenderingLayerType.BACKGROUND) {
            backgroundLayer = new SBRenderingLayer("System BG Layer", SBRenderingLayer.SBRenderingLayerType.BACKGROUND);
            backgroundLayer.setEditable(false);
            if (project.getBackgroundType() == SBBackgroundType.WHITE)
                backgroundLayer.addAction(new FillColorDrawAction(Color.WHITE));
            else if (project.getBackgroundType() == SBBackgroundType.TRANSPARENT){
                backgroundLayer.addAction(new FillPatternDrawAction(BitmapFactory.decodeResource(parent.getResources(), R.drawable.transparent_pattern)));
            }
            layers.add(0, backgroundLayer);
        }
        else
            backgroundLayer = layers.get(0);
    }

    public void addLayer() {
        CopyOnWriteArrayList<SBRenderingLayer> layers = project.getLayers();
        SBRenderingLayer layer = new SBRenderingLayer(String.format("Layer #%d", layers.size() - 2 + 1));
        layer.addAction(new FillColorDrawAction(Color.TRANSPARENT));
        layers.add(layer);
        renderingSurface.setLayers(layers);
    }

    public void selectLayer(int index){
        if (index < 1 || index >= project.getLayers().size()) return;
        project.getLayers().remove(tmpLayer);
        selectedLayer = project.getLayers().get(index);
        if (index < project.getLayers().size() - 1) {
            project.getLayers().add(index + 1, tmpLayer);
        }
        else
            project.getLayers().add(tmpLayer);

    }

    public SBTool selectTool(Class<? extends SBTool>  toolClass){
        SBTool tmp = availableTools.get(toolClass.getName());
        if (tool != null) {
            tmp.setColor(tool.getColor());
            tmp.setSize(tool.getSize());
        }
        tool = tmp;
        return tool;
    }

    public SBTool selectTool(int index) {
        if (index < 0 || index > availableTools.size()) return null;
        SBTool tmp = getAvailableTools()[index];
        if (tool != null) {
            tmp.setColor(tool.getColor());
            tmp.setSize(tool.getSize());
        }
        tool = tmp;
        return tool;
    }

    public boolean attachSurface(ViewGroup parent){
        if (parent == null) return false;
        if (renderingSurface != null && container != null) {
            detachSurface();
        }
        container = new RelativeLayout(parent.getContext());
        renderingSurface = new SBRenderingSurface(container.getContext());
        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        renderingSurface.setLayoutParams(params);
        container.addView(renderingSurface);
        renderingSurface.setOnTouchListener(this);
        this.parent = parent;
        this.parent.addView(container);
        initTools();
        return true;
    }



    public boolean detachSurface(){
        if (container != null && parent != null) {
            parent.removeView(container);
            container.removeView(renderingSurface);
            parent = null;
            container = null;
            renderingSurface = null;
            return true;
        }
        return false;
    }


    public SBTool[] getAvailableTools() {
        return availableTools.values().toArray(new SBTool[0]);
    }

    private void notifyPaint(){
        int i = 0;
        while (i < paintingManagerListeners.size()) {
            if (paintingManagerListeners.get(i) == null) paintingManagerListeners.remove(i);
            else{
                paintingManagerListeners.get(i).onPaintingComplete();
                ++i;
            }
        }
    }

    public void addProjectsManagerListeners(SBPaintingManagerListener paintingManagerListener) {
        this.paintingManagerListeners.add(paintingManagerListener);
    }

    public void removeProjectsManagerListeners(SBPaintingManagerListener paintingManagerListener) {
        this.paintingManagerListeners.remove(paintingManagerListener);
    }

       public interface SBPaintingManagerListener{
        void onPaintingComplete();
    }
}
