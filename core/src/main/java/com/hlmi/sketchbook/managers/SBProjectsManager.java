package com.hlmi.sketchbook.managers;

import android.content.Context;
import com.hlmi.sketchbook.helpers.StorageManager;
import com.hlmi.sketchbook.models.SBBackgroundType;
import com.hlmi.sketchbook.models.SBProject;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class SBProjectsManager {

    public static final String SAVE_DIR = "projects";
    public static final String EXPORT_DIR = "images";
    public static final String TUTORIALS_DIR = "tutorials";


    private ArrayList<SBProject> projects;
    private ArrayList<SBProject> tutorials;
    private SBProject currentProject;
    private ArrayList<SBProjectsManagerListener> projectsManagerListeners;

    private static SBProjectsManager manager;


    public static SBProjectsManager getManager(){
        if (manager == null) manager = new SBProjectsManager();
        return manager;
    }

    private SBProjectsManager() {
        projects = new ArrayList<>();
        tutorials = new ArrayList<>();
        projectsManagerListeners = new ArrayList<>();
    }

    public List<SBProject> getProjects() {
        return (List<SBProject>) projects.clone();
    }
    public List<SBProject> getTuroials() { return (List<SBProject>) tutorials.clone();}

    public void loadProjects() {
        List<SBProject> tmp = loadProjects(SAVE_DIR);
        if (tmp != null) {
            projects.clear();
            projects.addAll(tmp);
        }
    }

    public void loadTutorials(Context context){
        List<SBProject> tmp = loadProjects(TUTORIALS_DIR);
        if (tmp != null) {
            tutorials.clear();
            tutorials.addAll(tmp);
        }
        else{
            tutorials.clear();
            tutorials.addAll(SBTutorialBuilder.getBuilder().createTutorials(context));
            for (SBProject tut : tutorials) {
                saveProject(tut, TUTORIALS_DIR);
            }
        }
    }

    private ArrayList<SBProject> loadProjects(String from){
        ArrayList<SBProject> prj = new ArrayList<>();
        if (StorageManager.isExternalStorageReadable()) {
            File[] files = StorageManager.openExternalDirectory(from).listFiles();
            if (files != null && files.length > 0) {  // files == null means no files in the folder
                for (int i = 0; i < files.length; i++) {
                    String[] parts = files[i].getName().split("\\.");
                    if (SBProject.EXTENSION_PROJ.equals("." + parts[parts.length - 1])) {
                        prj.add(loadProject(files[i]));
                    }
                }
                int k = 0;
                return prj;
            }
        }
        return null;

    }



    public SBProject createProject(String name, int width, int height, SBBackgroundType backgroundType) {
        SBProject project = new SBProject(name, width, height, backgroundType);
        projects.add(project);
        saveProject(project);
        return project;
    }

    public SBProject setCurrentProject(int index){
        if (index < 0 || index >= projects.size())
            currentProject = null;
        else
           currentProject = projects.get(index);
        return currentProject;
    }

    public SBProject setCurrentTutorial(int index) {
        if (index < 0 || index >= tutorials.size())
            currentProject = null;
        else
            currentProject = tutorials.get(index);
        return currentProject;
    }


    public void setCurrentProject(SBProject project) {
        int i = projects.indexOf(project);
        if (i >= 0)
           setCurrentProject(i);
    }

    public SBProject getCurrentProject() {
        return currentProject;
    }

    public void saveProject() {
        saveProject(currentProject);
    }

    public void saveProject(SBProject project) {
        saveProject(project, SAVE_DIR);
    }

    private void saveProject(SBProject project, String dir){
        if (project != null){
            if (StorageManager.isExternalStorageWritable()) {
                try {
                    FileOutputStream fos = new FileOutputStream(StorageManager.createDocumentFile(StorageManager.openExternalDirectory(dir), getProjectFilename(project)));
                    ObjectOutputStream os = new ObjectOutputStream(fos);
                    os.writeObject(project);
                    os.close();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public SBProject loadProject(String filename){
        return loadProject(new File(StorageManager.openExternalDirectory(SAVE_DIR), filename));
    }

    public SBProject loadProject(File f){
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f));
            return (SBProject) ois.readObject();
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean removeProject(int currentProjectIndex) {
        if (currentProjectIndex < 0 || currentProjectIndex >= projects.size()) return false;
        else {
            SBProject project = projects.get(currentProjectIndex);
            File f = new File(StorageManager.openExternalDirectory(SAVE_DIR), getProjectFilename(project));
            if (f.delete()) {
                projects.remove(currentProjectIndex);
                notifyProjectRemoved(currentProjectIndex, project);
                return true;
            } else return false;
        }
    }

    public boolean renameProject(int currentProjectIndex, String newName) {
        if (currentProjectIndex < 0 || currentProjectIndex >= projects.size() || hasProjectNamed(newName)) return false;
        else {
            SBProject project = projects.get(currentProjectIndex);
            String old = project.getName();
            File f = new File(StorageManager.openExternalDirectory(SAVE_DIR), getProjectFilename(project));
            if (f.delete()) {
                project.setName(newName);
                saveProject(project);
                notifyProjectRenamed(currentProjectIndex, old, project);
                return true;
            }
            else return false;
        }
    }

    public String getProjectFilename(SBProject project){
        return project.getName().toLowerCase().replace(" ", "_") + SBProject.EXTENSION_PROJ;
    }

    public String getProjectImageFilename(SBProject project){
        return project.getName().toLowerCase().replace(" ", "_") + SBProject.EXTENSION_PNG;
    }


    private void notifyProjectRenamed(int index, String oldName, SBProject project){
        int i = 0;
        while (i < projectsManagerListeners.size()) {
            if (projectsManagerListeners.get(i) == null) projectsManagerListeners.remove(i);
            else{
                projectsManagerListeners.get(i).onProjectRenamed(index, oldName, project);
                ++i;
            }
        }
    }

    private void notifyProjectRemoved(int index, SBProject project){
        int i = 0;
        while (i < projectsManagerListeners.size()) {
            if (projectsManagerListeners.get(i) == null) projectsManagerListeners.remove(i);
            else{
                projectsManagerListeners.get(i).onProjectRemoved(index,  project);
                ++i;
            }

        }
    }


    public void addProjectsManagerListeners(SBProjectsManagerListener projectsManagerListener) {
        this.projectsManagerListeners.add(projectsManagerListener);
    }

    public void removeProjectsManagerListeners(SBProjectsManagerListener projectsManagerListener) {
        this.projectsManagerListeners.remove(projectsManagerListener);
    }

    public boolean hasProjectNamed(String name) {
        for (SBProject project: projects) {
            if (project.getName().equals(name))
                return true;
        }
        return false;
    }

    public interface SBProjectsManagerListener{
        void onProjectRemoved(int index, SBProject project);

        void onProjectRenamed(int index, String oldName, SBProject project);
    }
}
