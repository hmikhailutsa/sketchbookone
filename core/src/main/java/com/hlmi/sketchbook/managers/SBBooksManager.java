package com.hlmi.sketchbook.managers;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;
import com.hlmi.sketchbook.core.R;
import com.hlmi.sketchbook.helpers.StorageManager;
import com.hlmi.sketchbook.models.SBBook;
import com.hlmi.sketchbook.models.SBBook.SBBookCategory;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class SBBooksManager {

    private static final String BOOKS_DIR = "books";

    private HashMap<SBBookCategory, ArrayList<SBBook>> shelf;

    private static SBBooksManager manager;
    public static SBBooksManager getManager(){
        if (manager == null) manager = new SBBooksManager();
        return manager;
    }

    private SBBooksManager() {
        shelf = new HashMap<>();
        for (SBBookCategory cat : SBBookCategory.values()) {
            registerCategory(cat);
        }
        loadBooks();
    }

    private void registerCategory(SBBookCategory category){
        shelf.put(category, new ArrayList<SBBook>());
        StorageManager.openExternalDirectory(BOOKS_DIR + "//" + category.toString());
    }

    private void loadBooks(){
        ArrayList<SBBook> books = shelf.get(SBBookCategory.Basics);
        books.add(new SBBook("Наука о цвете и живопись", "Зайцев А.С.", R.drawable.books_b1, "books_b1.pdf", SBBookCategory.Basics, R.color.lightRed, R.color.white));
        books.add(new SBBook("Основы рисунка", "Шембель А.Ф.", R.drawable.books_b2, "books_b2.pdf", SBBookCategory.Basics, R.color.lightYellow, R.color.main_text));
        books.add(new SBBook("Золотое сечение", "Ковалев Ф.В.", R.drawable.books_b3, "books_b3.pdf", SBBookCategory.Basics, R.color.darkGreen, R.color.white));
        books.add(new SBBook("Цвет в живописи", "Волков Н.Н.", R.drawable.books_b4, "books_b4.pdf", SBBookCategory.Basics, R.color.darkGreen, R.color.white));
        books.add(new SBBook("Искусство цвета", "Иоханнес Иттен", R.drawable.books_b5, "books_b5.pdf", SBBookCategory.Basics, R.color.red_books, R.color.white));
        books.add(new SBBook("Рисунок и перспектива", "Федоров М.В.", R.drawable.books_b6, "books_b6.pdf", SBBookCategory.Basics, R.color.lightGray_books, R.color.white));

        books = shelf.get(SBBookCategory.Techniques);
        books.add(new SBBook("Как рисовать пастелью", "Максимова О.С.", R.drawable.books_t1, "books_t1.pdf", SBBookCategory.Techniques, R.color.lightRed, R.color.white));
        books.add(new SBBook("Пейзаж и масло", "Виннер А.В.", R.drawable.books_t2, "books_t2.pdf", SBBookCategory.Techniques, R.color.lightViolet_books, R.color.main_text));
        books.add(new SBBook("Техника акварели", "Ревякин П.П.", R.drawable.books_t3, "books_t3.pdf", SBBookCategory.Techniques, R.color.lightGray_books, R.color.white));
        books.add(new SBBook("Каллиграфия", "Богдеско И.", R.drawable.books_t4, "books_t4.pdf", SBBookCategory.Techniques, R.color.lightViolet_books, R.color.white));

        books = shelf.get(SBBookCategory.History);
        books.add(new SBBook("Эпоха Возрождения", "Булавина А.В.", R.drawable.books_h1, "books_h1.pdf", SBBookCategory.History, R.color.red_books, R.color.white));
        books.add(new SBBook("Пабло Пикассо", "Жидель Анри", R.drawable.books_h2, "books_h2.pdf", SBBookCategory.History, R.color.lightBlue_books, R.color.main_text));
        books.add(new SBBook("Золотое сечение", "Ковалев Ф.В.", R.drawable.books_h3, "books_h3.pdf", SBBookCategory.History, R.color.darkGreen, R.color.white));
        books.add(new SBBook("Айвазовский", "Барсамов Н.Н.", R.drawable.books_h4, "books_h4.pdf", SBBookCategory.History, R.color.lightGray_books, R.color.white));
    }

    public ArrayList<SBBook> getBooks(SBBookCategory category) {
        return (ArrayList<SBBook>) shelf.get(category).clone();
    }

    public boolean isAvailableBook(SBBook book){
        return new File(StorageManager.openExternalDirectory(BOOKS_DIR + File.separator + book.getCategory().toString()), book.getFilename()).exists();
    }

    public boolean openBook(Context context, SBBookCategory category, int index){
        ArrayList<SBBook> books = shelf.get(category);
        if (index < 0 || index >= books.size()) return false;
        SBBook book = books.get(index);
        File file = new File(StorageManager.openExternalDirectory(BOOKS_DIR + File.separator + category.toString()), book.getFilename());

        if (file.exists()) {
            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            try {
                context.startActivity(intent);
                return true;
            }
            catch (ActivityNotFoundException e) {
                Toast.makeText(context, "No Application Available to View PDF", Toast.LENGTH_SHORT).show();
            }

        }
        return false;
    }
}
