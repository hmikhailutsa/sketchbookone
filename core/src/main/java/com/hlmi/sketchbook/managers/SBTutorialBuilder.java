package com.hlmi.sketchbook.managers;

import android.content.Context;
import com.hlmi.sketchbook.actions.BitmapDrawAction;
import com.hlmi.sketchbook.core.R;
import com.hlmi.sketchbook.helpers.ImageLoader;
import com.hlmi.sketchbook.helpers.SBPoint;
import com.hlmi.sketchbook.models.SBBackgroundType;
import com.hlmi.sketchbook.models.SBProject;
import com.hlmi.sketchbook.rendering.SBRenderingLayer;

import java.util.ArrayList;
import java.util.Arrays;

public class SBTutorialBuilder {
    private static SBTutorialBuilder manager = new SBTutorialBuilder();

    public static SBTutorialBuilder getBuilder() {
        return manager;
    }



    private SBTutorialBuilder() {}

    public ArrayList<SBProject> createTutorials(Context context) {
        ArrayList<SBProject> tutorials = new ArrayList<>();



        tutorials.add(createTutorial1(context));
        tutorials.add(createTutorial2(context));
        tutorials.add(createTutorial3(context));
        return tutorials;
    }

    private SBProject createTutorial1(Context context){
        SBProject tut = new SBProject("Cecelia Tutorials", 1500, 900, SBBackgroundType.WHITE, ImageLoader.decodeSampledBitmap(context, R.drawable.t1_6, 300, 400), false);

        SBRenderingLayer[] layers = new SBRenderingLayer[]{
                new SBRenderingLayer("Sketching"),
                new SBRenderingLayer("Classic look"),
                new SBRenderingLayer("Base colors"),
                new SBRenderingLayer("Facial features"),
                new SBRenderingLayer("Hair"),
                new SBRenderingLayer("Detaling")
        };
        layers[0].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t1_1, 300, 400), new SBPoint(0, 0)));
        layers[1].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t1_2, 300, 400), new SBPoint(0, 0)));
        layers[2].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t1_3, 300, 400), new SBPoint(0, 0)));
        layers[3].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t1_4, 300, 400), new SBPoint(0, 0)));
        layers[4].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t1_5, 300, 400), new SBPoint(0, 0)));
        layers[5].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t1_6, 300, 400), new SBPoint(0, 0)));
        for (int i = 1; i < layers.length; i++) {
            layers[i].setEnabled(false);
        }
        tut.addLayers(Arrays.asList(layers), false);
        tut.addLayer(new SBRenderingLayer("Start", SBRenderingLayer.SBRenderingLayerType.CUSTOM), true);
        return tut;
    }

    private SBProject createTutorial2(Context context) {
        SBProject tut = new SBProject("Orange Tutorials", 1500, 900, SBBackgroundType.WHITE, ImageLoader.decodeSampledBitmap(context, R.drawable.t2_9, 300, 400), false);

        SBRenderingLayer[] layers = new SBRenderingLayer[]{
                new SBRenderingLayer("Step 1"),
                new SBRenderingLayer("Step 2"),
                new SBRenderingLayer("Step 3"),
                new SBRenderingLayer("Step 4"),
                new SBRenderingLayer("Step 5"),
                new SBRenderingLayer("Step 6"),
                new SBRenderingLayer("Step 7"),
                new SBRenderingLayer("Step 8"),
                new SBRenderingLayer("Step 9")
        };
        layers[0].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t2_1, 300, 400), new SBPoint(0, 0)));
        layers[1].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t2_2, 300, 400), new SBPoint(0, 0)));
        layers[2].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t2_3, 300, 400), new SBPoint(0, 0)));
        layers[3].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t2_4, 300, 400), new SBPoint(0, 0)));
        layers[4].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t2_5, 300, 400), new SBPoint(0, 0)));
        layers[5].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t2_6, 300, 400), new SBPoint(0, 0)));
        layers[6].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t2_7, 300, 400), new SBPoint(0, 0)));
        layers[7].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t2_8, 300, 400), new SBPoint(0, 0)));
        layers[8].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t2_9, 300, 400), new SBPoint(0, 0)));
        for (int i = 1; i < layers.length; i++) {
            layers[i].setEnabled(false);
        }
        tut.addLayers(Arrays.asList(layers), false);
        tut.addLayer(new SBRenderingLayer("Start", SBRenderingLayer.SBRenderingLayerType.CUSTOM), true);
        return tut;
    }

    private SBProject createTutorial3(Context context) {
        SBProject tut = new SBProject("Horse Tutorials", 1500, 900, SBBackgroundType.WHITE, ImageLoader.decodeSampledBitmap(context, R.drawable.t3_6, 300, 400), false);

        SBRenderingLayer[] layers = new SBRenderingLayer[]{
                new SBRenderingLayer("Base lines 1"),
                new SBRenderingLayer("Base lines 2"),
                new SBRenderingLayer("Base lines 3"),
                new SBRenderingLayer("Anatomy"),
                new SBRenderingLayer("Muscles"),
                new SBRenderingLayer("Detaling")
        };
        layers[0].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t3_1, 300, 400), new SBPoint(0, 0)));
        layers[1].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t3_2, 300, 400), new SBPoint(0, 0)));
        layers[2].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t3_3, 300, 400), new SBPoint(0, 0)));
        layers[3].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t3_4, 300, 400), new SBPoint(0, 0)));
        layers[4].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t3_5, 300, 400), new SBPoint(0, 0)));
        layers[5].addAction(new BitmapDrawAction(ImageLoader.decodeSampledBitmap(context, R.drawable.t3_6, 300, 400), new SBPoint(0, 0)));
        for (int i = 1; i < layers.length; i++) {
            layers[i].setEnabled(false);
        }
        tut.addLayers(Arrays.asList(layers), false);
        tut.addLayer(new SBRenderingLayer("Start", SBRenderingLayer.SBRenderingLayerType.CUSTOM), true);
        return tut;
    }
}
