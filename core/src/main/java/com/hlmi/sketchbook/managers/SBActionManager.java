package com.hlmi.sketchbook.managers;

import com.hlmi.sketchbook.actions.Action;
import com.hlmi.sketchbook.helpers.LimitedList;

public class SBActionManager {

    private static final int NO_ACTIONS = -1;
    private static final int DEFAULT_CAPACITY = 100;

    private static SBActionManager manager;
    public static SBActionManager getManager() {
        if (manager == null) manager = new SBActionManager();
        return manager;
    }

    private LimitedList<Action> actions;

    private int capacity;
    private int currentActionIndex;

    private SBActionManager() {
        capacity = DEFAULT_CAPACITY;
        currentActionIndex = NO_ACTIONS;
        actions = new LimitedList<>(capacity);
    }

    public int getCapacity() {
        return capacity;
    }

    public int getExecutedActionsCount(){
        return actions.size();
    }


    public void setCapacity(int capacity) {
        this.capacity = capacity;
        actions.extend(this.capacity);
    }

    private boolean hasRolledBackActions(){
        return currentActionIndex < actions.size() - 1;
    }

    public boolean isRollbackAvailable(){
        return currentActionIndex != NO_ACTIONS;
    }

    public boolean isExecuteAvailable(){
        return hasRolledBackActions();
    }

    public void executeAction(Action action){
        if (action == null) return;
        action.execute();
        if (hasRolledBackActions()) {
            actions.removeRange(currentActionIndex + 1, actions.size());
            currentActionIndex = actions.size() - 1;
        }
        if (actions.add(action))
            ++currentActionIndex;
    }

    public void executeLastAction(){
        if (hasRolledBackActions())
            actions.get(++currentActionIndex).execute();
    }

    public void rollbackLastAction() {
        if (currentActionIndex == NO_ACTIONS) return;
        actions.get(currentActionIndex--).rollback();
    }

    public void rollbackLastActions(int numberOfActions){
        if (numberOfActions < 0)
            numberOfActions = actions.size();
        while (numberOfActions-- > 0 && currentActionIndex != NO_ACTIONS)
            rollbackLastAction();
    }

    public int getCurrentActionIndex() {
        return currentActionIndex;
    }

    public String[] getHistory(){
        String[] history = new String[actions.size()];
        for (int i = 0; i < history.length; i++) {
            history[i] = actions.get(i).getDescription();
        }
        return history;
    }

    public void clear() {
        actions.clear();
        currentActionIndex = NO_ACTIONS;
    }
}
