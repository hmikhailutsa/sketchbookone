package com.hlmi.sketchbook.helpers;

import android.os.Build;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.IOException;

public class StorageManager {

    public static final String APP_NAME = "SketchBook";
    public static final String DOCS_DIR = "Documents";

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }

    public static File createDocumentFile(String filename) {
        return createDocumentFile(openExternalDirectory(), filename);
    }

    public static File createDocumentFile(File dir, String filename){
        File file = new File(dir, filename);
        try {
            if (!file.createNewFile()) {
                Log.e("StorageManager", "Directory not created");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static File openExternalDirectory() {
        return openExternalDirectory(null);
    }

    public static File openExternalDirectory(String appendDirs) {
        File dir;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getPath() + File.separator + APP_NAME + ((appendDirs != null && !appendDirs.isEmpty()) ? File.separator + appendDirs : ""));
        else
            dir = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + DOCS_DIR + File.separator + APP_NAME + ((appendDirs != null && !appendDirs.isEmpty()) ? File.separator + appendDirs : ""));
        if (dir.mkdirs())
            Log.d("StorageManager", "DIRS CREATED: " + dir.getPath());
        else Log.d("StorageManager", "FAILED: " + dir.getPath());
        return dir;
    }

    public static File openInternalDirectory() {
        return openInternalDirectory(null);
    }

    public static File openInternalDirectory(String appendDirs) {
        File dir = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + APP_NAME + ((appendDirs != null && !appendDirs.isEmpty()) ? File.separator + appendDirs : ""));
        if (dir.mkdirs())
            Log.d("StorageManager", "DIRS CREATED: " + dir.getPath());
        else Log.d("StorageManager", "FAILED: " + dir.getPath());
        return dir;
    }
}
