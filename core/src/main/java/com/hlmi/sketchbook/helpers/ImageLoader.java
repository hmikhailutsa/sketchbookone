package com.hlmi.sketchbook.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.DrawableRes;

/** Safety loads image from path. Samples it to specified size or to fit memory. */
public class ImageLoader {

    private ImageLoader() {}

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    || (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private static Bitmap decodeFileSafe(String imagePath, int samples) {
        Bitmap safeBitmap = null;
        boolean isLoaded = false;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = samples;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        while (!isLoaded){
            try {
                if (safeBitmap!=null) safeBitmap.recycle();
                safeBitmap = BitmapFactory.decodeFile(imagePath, options);
                isLoaded = true;
            } catch (OutOfMemoryError e) {
                isLoaded = false;
                options.inSampleSize*=2;
            }
        }
        return safeBitmap;
    }
    private static Bitmap decodeFileSafe(Context context, @DrawableRes int res, int samples) {
        Bitmap safeBitmap = null;
        boolean isLoaded = false;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = samples;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        while (!isLoaded){
            try {
                if (safeBitmap!=null) safeBitmap.recycle();
                safeBitmap = BitmapFactory.decodeResource(context.getResources(), res);
                isLoaded = true;
            } catch (OutOfMemoryError e) {
                isLoaded = false;
                options.inSampleSize*=2;
            }
        }
        return safeBitmap;
    }

    public static Bitmap decodeSampledBitmap(String filename, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filename, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return decodeFileSafe(filename, options.inSampleSize);
    }
    public static Bitmap decodeSampledBitmap(Context context, @DrawableRes int res, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(context.getResources(), res);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return decodeFileSafe(context, res, options.inSampleSize);
    }

}
