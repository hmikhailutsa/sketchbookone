package com.hlmi.sketchbook.helpers;

import java.util.ArrayList;
import java.util.Collection;

public class LimitedList<T> extends ArrayList <T> {

    private int capacity;

    public LimitedList(int capacity) {
        this.capacity = capacity;
    }

    public LimitedList(Collection<? extends T> collection, int capacity) {
        this.capacity = capacity;
        addAll(collection);
    }

    public void extend(int newCapacity) {
        if (newCapacity < 0) return;
        this.capacity = newCapacity;
        if (size() > newCapacity) {
            for (int i = newCapacity - 1; i < size(); i++) {
                remove(i);
            }
        }
    }

    public int getCapacity() {
        return capacity;
    }

    private boolean checkCapacity(){
        boolean removed = false;
        while (size() > capacity){ remove(0); removed = true;}
        return removed;
    }

    @Override
    public boolean add(T t) {
        super.add(t);
        return !checkCapacity();
    }

    /// Limited list doesn't support insertion.
    @Override
    public void add(int index, T object) {
        super.add(object);
        checkCapacity();
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        super.addAll(collection);
        return !checkCapacity();
    }

    /// Limited list doesn't support insertion.
    @Override
    public boolean addAll(int index, Collection<? extends T> collection) {
        super.addAll(collection);
        return !checkCapacity();
    }

    public void removeRange(int fromIndex, int toIndex){
        super.removeRange(fromIndex, toIndex);
    }
}
