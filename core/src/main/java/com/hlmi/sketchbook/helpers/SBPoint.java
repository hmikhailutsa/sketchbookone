package com.hlmi.sketchbook.helpers;

import java.io.Serializable;

public class SBPoint implements Comparable, Serializable{

    public float x,y;

    public SBPoint(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public SBPoint() {}

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof SBPoint)) return -1;
        SBPoint p = ((SBPoint) o);
        if (x == p.x && y == p.y) return 0;
        else return 1;
    }
}
